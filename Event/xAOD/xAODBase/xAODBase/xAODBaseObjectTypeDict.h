// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: xAODBaseDict.h 618909 2014-09-29 10:16:52Z krasznaa $
#ifndef XAODBASE_XAODBASEOBJECTTYPEDICT_H
#define XAODBASE_XAODBASEOBJECTTYPEDICT_H

// Local include(s):
#include "xAODBase/ObjectType.h"

#endif // XAODBASE_XAODBASEOBJECTTYPEDICT_H
